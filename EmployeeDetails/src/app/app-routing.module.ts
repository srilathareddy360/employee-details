import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddEmployeeDetailsComponent } from './add-employee-details/add-employee-details.component';
import { EmployeeDashboardComponent } from './employee-dashboard/employee-dashboard.component';
// import { Add-AddEmployeeDetailsComponent } from './add-employees/add-employees.component';

const routes: Routes = [
  
 
   { path: "", component: EmployeeDashboardComponent },
   { path: "Add", component: AddEmployeeDetailsComponent },
 
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
